# Bugs & issues in the original

## App.tsx

- `useEffect` doesn't contain second argument, list of deps, so it will execute on each render
- `useEffect` should preferably contain return function, that will cancel async action, so it won't update components if it's not mounted
- `Todo` component should have `key` property passed with unique key. `id` can be used if we are sure it's gonna be unique
- If the response from API is full (without "pagination") we should set it as whole with `setState` and not append
- If the response is paginaged, it'd still better to check for duplicities and merge accordingly. Also in StrictMode and DEV env useEffect is fired twice, so we get duplicities
- Using globally scoped `var` is bad practice and should be always avoided
- `for loop` could be switched to `forEach` array function
- When using setState with Previous state, it should be look like this: `setState((prev) => [...prev, awaitedTodos[i]]);` or ideally without uncessary loop, so `setState((prev) => [...prev, ...awaitedTodos]);`

## Todo.tsx

- Unecessary <div> inside another <div>
- `shouldComponentUpdate` is comparing object reference, that will be different on each rerender, so it triggers unecessary renders. It should do at least shallow comparision
- `!=` should be avoided as it's not that strict as `!==`
- `handleOnClick` not doint anything special, but I guess it's just prepared for finished task with routing
- no typescript type declaration, only `any`

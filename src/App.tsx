import { useContext } from "react";
import { RouterProvider } from "react-router-dom";
import { ApiContext, ApiProvider } from "./other/ApiContext";
import { router } from "./other/routes";

function App() {
  const { todos, loading } = useContext(ApiContext);

  console.log(todos, loading);

  return (
    <ApiProvider>
      <main style={{ padding: "1rem" }}>
        <RouterProvider router={router} />
      </main>
    </ApiProvider>
  );
}

export default App;

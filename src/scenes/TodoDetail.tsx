import { useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { ApiContext } from "../other/ApiContext";
import { routes } from "../other/routes";

function TodoDetail() {
  const { todoId } = useParams();
  const { todos } = useContext(ApiContext);

  const foundTodo =
    todoId && todos.find((todo) => todo.id === parseInt(todoId));

  return (
    <div>
      <h1>Todo Detail ID: {todoId}</h1>
      {foundTodo ? (
        <p>{foundTodo.title}</p>
      ) : (
        <div>
          <p style={{ color: "red" }}>
            Requested todo with ID: {todoId} was not found
          </p>
        </div>
      )}
      <Link to={routes.HOME}>Go back</Link>
    </div>
  );
}

export default TodoDetail;

import { useContext } from "react";
import { ApiContext } from "../other/ApiContext";
import TodoTable from "../components/TodoTable";

function Home() {
  const { todos, loading } = useContext(ApiContext);

  return (
    <div>
      <h1>Todo List</h1>
      <p>Select todo to see its' details:</p>
      {loading && <h6 style={{ color: "blue" }}>Loading todos...</h6>}
      <TodoTable todos={todos} />
    </div>
  );
}

export default Home;

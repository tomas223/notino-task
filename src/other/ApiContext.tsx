import { createContext, ReactNode, useEffect, useMemo, useState } from "react";
import { TodoType } from "../types";

type ApiContextProps = {
  todos: TodoType[];
  loading: boolean;
};

type ApiProviderProps = {
  children: ReactNode;
};

const DEFAULT_CONTEXT = { todos: [], loading: false };
const API_URL_BASE = "https://jsonplaceholder.typicode.com/todos";

const ApiContext = createContext<ApiContextProps>(DEFAULT_CONTEXT);

function ApiProvider(props: ApiProviderProps) {
  const { children } = props;

  const [loading, setLoading] = useState(DEFAULT_CONTEXT.loading);
  const [todos, setTodos] = useState<ApiContextProps["todos"]>(
    DEFAULT_CONTEXT.todos
  );

  useEffect(() => {
    const fetchTodos = async () => {
      setLoading(true);
      const data = await fetch(API_URL_BASE);
      const json = await data.json();

      console.log(data);
      setTodos(json);
      setLoading(false);
    };

    fetchTodos().catch((err) => {
      console.log(err);
      setLoading(false);
    });
  }, []);

  const value = useMemo(
    () => ({
      todos,
      loading,
    }),
    [todos, loading]
  );

  return <ApiContext.Provider value={value}>{children}</ApiContext.Provider>;
}

export { ApiProvider, ApiContext };

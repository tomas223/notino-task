import { createBrowserRouter } from "react-router-dom";
import Home from "../scenes/Home";
import TodoDetail from "../scenes/TodoDetail";

enum routes {
  HOME = "/",
  DETAIL = "detail",
}

const router = createBrowserRouter([
  {
    path: routes.HOME,
    element: <Home />,
  },
  {
    path: `${routes.DETAIL}/:todoId`,
    element: <TodoDetail />,
  },
]);

export { router, routes };

import React from "react";

type Props = {
  todo: { id: string; title: string };
};

class Todo extends React.Component<Props> {
  shouldComponentUpdate(prevProps: Props) {
    const { todo } = this.props;
    const { todo: prevTodo } = prevProps;
    return todo.id !== prevTodo.id || todo.title !== prevTodo.title;
  }

  handleOnClick() {
    window.location.href = "/detail";
  }

  render() {
    return (
      <div>
        <div onClick={this.handleOnClick}>{this.props.todo.title}</div>
      </div>
    );
  }
}

export default Todo;

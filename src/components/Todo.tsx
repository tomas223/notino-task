import { useNavigate } from "react-router-dom";
import { routes } from "../other/routes";
import { TodoType } from "../types";

type Props = TodoType;

function Todo(props: Props) {
  const { id, title } = props;
  const navigate = useNavigate();

  const handleOnClick = (todoId: number) => {
    navigate(`${routes.DETAIL}/${todoId}`);
  };

  return (
    <tr role="button" onClick={() => handleOnClick(id)} key={id}>
      <th scope="row">{id}</th>
      <td>{title}</td>
    </tr>
  );
}

export default Todo;

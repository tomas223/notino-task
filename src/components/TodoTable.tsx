import { Table } from "reactstrap";
import Todo from "./Todo";
import { TodoType } from "../types";

type Props = {
  todos: TodoType[];
};

function TodoTable(props: Props) {
  const { todos } = props;
  return (
    <Table hover striped>
      <thead>
        <tr>
          <th>#</th>
          <th>title</th>
        </tr>
      </thead>
      <tbody>
        {todos.map(({ id, title }) => (
          <Todo key={id} id={id} title={title} />
        ))}
      </tbody>
    </Table>
  );
}

export default TodoTable;
